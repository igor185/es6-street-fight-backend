FROM node:10

WORKDIR .
COPY package*.json ./
COPY . /

RUN npm install

CMD ["node", "./bin/www"]
EXPOSE $PORT