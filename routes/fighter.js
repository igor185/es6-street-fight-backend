const router = require('express').Router();
const storage = require('../storage/main');


// listing  /fighter  

// GET all fighters
router.get('/', (req, res) => {
  storage.getFighters()
    .then(data => res.send({
      content: data
    }))
    .catch((error) => {
      console.log(error);
      res.status(404);
      res.send({
        message: error.message
      });
    })
});

// GET details fighter by id
router.get('/:id', function (req, res, next) {
  storage.getFighter(req.params.id)
    .then(data => res.send({
      content: data
    }))
    .catch((error) => {
      console.log(error);
      res.status(404);
      res.send({
        message: error.message
      });
    })
});

// POST new fighter and return ID
router.post('/', (req, res) => {
  storage.createFighter(req.body)
    .then(data => {
      res.status(200);
      res.send({
        content: data
      });
    })
    .catch(err => {
      res.status(400);
      res.send({
        message: err.message
      });
    });
});

// PUT fighter by id
router.put('/:id', (req, res) => {
  storage.updateFighter(req.body, req.params.id)
    .then((data) => {
      res.status(200);
      res.send({
        content: data
      });
    })
    .catch((err) => {
      res.status(400);
      res.send({
        message: err.message
      });
    })
});

// DELETE fighter at all
router.delete('/:id', (req, res) => {
  storage.deleteFighter(req.params.id)
    .then((data) => {
      res.status(200).send();
    })
    .catch((err) => {
      console.log(err);
      res.status(523);
      res.send({
        message: err.message
      })
    });
});

module.exports = router;