# Street Fighter

## Hosting
- [Heroku app](https://igor-babin.herokuapp.com/)


## Installation

- Clone этот репозиторий
- npm install
- npm run start
- http://localhost:3000

## API

- Открыть игру:  GET /

        Возвращает html документ

- Получить всех бойцов: GET /fighter

        Возвращает json объект, в свойстве content находиться массив бойцов

- Получить бойца по id: GET /fighter/id

        Возвращает json, объект в свойстве content находиться боец
        
- Создать бойца: POST /fighter. В теле запроса должно быть :
    - name
    - source (url к картинке с бойцом)
    - attack (Может быть как число так и строка с числом)
    - defense (Аналогично attack)
    - health (Аналогично attack)


          Возвращает json объект, в свойстве content находиться созданный боец с генерированным свойством _id


- Изменить параметр бойца: PUT /fighter/id. В теле запроса должен быть какой-то из параметров в POST /fighter методе.

        Возвращает json объект в свойстве content находиться объект, который был изменен или пустой объект, если ничего не изменено


- Удалить пользователя DELETE /fighter/id


В случае ошибки вернеться объект со свойством message.

