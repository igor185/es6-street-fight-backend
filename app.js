var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var fighterRouter = require('./routes/fighter');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/fighter', fighterRouter);

app.use((req, res) => {
    res.status(404);
    res.send({
        beginPlay: 'GET /',
        getFighters: 'GET /fighter',
        getFighter: 'GET /fighter:id',
        createFighter: 'POST /fighter/:id',
        updateFighter: 'PUT /fighter/:id',
        deleteFighter: 'DELETE /fighter/:id'
    })
})

module.exports = app;