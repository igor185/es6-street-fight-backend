const fs = require("fs");

module.exports = (id) => {
    return new Promise((resolve, rejective) => {
        fs.readFile(`./storage/data/details/fighter/${id}.json`, 'utf-8', (err, data) => {
            if (err) return rejective(err);
            resolve(JSON.parse(data));
        })
    })
}