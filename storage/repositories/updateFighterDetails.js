const dirFigtersDetails = './storage/data/details/fighter'
const fs = require("fs");
const writeValueInObj = require('../helpers/writeValueInObj');
const validateNumber = require('../helpers/validateNumber');


module.exports = (id, name, source, attack, defense, health) => {
    return new Promise((resolve, rejective) => {
        if (!(name || source || attack || defense || health))
            return resolve();
        if (validateNumber(attack) || validateNumber(defense) || validateNumber(health))
            return rejective(new Error('Not valid data'));

        fs.readFile(`${dirFigtersDetails}/${id}.json`, 'utf-8', (err, data) => {
            if (err) return rejective(err);


            data = JSON.parse(data);

            writeValueInObj(data, 'name', name);
            writeValueInObj(data, 'source', source);
            writeValueInObj(data, 'attack', +attack);
            writeValueInObj(data, 'defense', +defense);
            writeValueInObj(data, 'health', +health);

            fs.writeFile(`${dirFigtersDetails}/${id}.json`, JSON.stringify(data), (err) => {
                if (err) return rejective(err);
                resolve(data);
            });
        })
    })
};