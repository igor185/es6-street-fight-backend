const dirFigtersDetails = './storage/data/details/fighter'
const fs = require("fs");


module.exports = (fighter) => {
    return new Promise((resolve, rejective) => {
        fs.writeFile(`${dirFigtersDetails}/${fighter._id}.json`, JSON.stringify(fighter), (err) => {
            if (err) return rejective(err);
            resolve(fighter);
        });
    });
}