const dirFighters = './storage/data/fighters.json'
const fs = require("fs");

module.exports = (fighter) => {
    return new Promise((resolve, rejective) => {
        fs.readFile(dirFighters, 'utf-8', (err, data) => {
            if (err) return rejective(err);
            let arr = JSON.parse(data);
            let {
                name,
                source,
                _id
            } = fighter;
            arr.push({
                name,
                source,
                _id
            });
            fs.writeFile(dirFighters, JSON.stringify(arr), (err) => {
                if (err) return rejective(err);
                resolve();
            });
        })
    })
}