const dirFighters = './storage/data/fighters.json'
const fs = require("fs");
const findIndex = require('../helpers/findIndex');

module.exports = (id) => {
    return new Promise((resolve, rejective) => {
        fs.readFile(dirFighters, 'utf-8', (err, data) => {
            if (err) return rejective(err);
            let arr = JSON.parse(data);
            let index = findIndex(arr, id);
            arr.splice(index, 1)
            if (index == -1) return rejective(new Error('Not found'));
            fs.writeFile(dirFighters, JSON.stringify(arr), (err) => {
                if (err) return rejective(err);
                resolve();
            });
        });
    })
}