const dirFigtersDetails = './storage/data/details/fighter'
const fs = require("fs");

module.exports = (id) => {
    return new Promise((resolve, rejective) => {
        fs.readFile(`${dirFigtersDetails}/${id}.json`, function (err, data) {
            if (err) rejective(err);
            let fighter = JSON.parse(data)
            fs.unlink(`${dirFigtersDetails}/${id}.json`, function (err) {
                if (err) return rejective(err);
                resolve(fighter);
            });
        });
    })
}