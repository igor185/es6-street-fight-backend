const fs = require("fs");
const dirFighters = './storage/data/fighters.json'

module.exports = () => {
    return new Promise((resolve, rejective) => {
        fs.readFile(dirFighters, 'utf-8', (err, data) => {
            if (err) return rejective(err);
            resolve(JSON.parse(data));
        })
    })
}