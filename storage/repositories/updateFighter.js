const dirFighters = './storage/data/fighters.json'
const fs = require("fs");
const findIndex = require('../helpers/findIndex');
const writeValueInObj = require('../helpers/writeValueInObj');


module.exports = (id, name, source) => {
    return new Promise((resolve, rejective) => {
        if (!(name || source))
            return resolve();

        console.log('work');
        fs.readFile(dirFighters, 'utf-8', (err, data) => {
            if (err) return rejective(err);

            data = JSON.parse(data);
            let index = findIndex(data, id);
            if (index == -1) return rejective();
            let fighter = data[index];

            writeValueInObj(fighter, 'name', name);
            writeValueInObj(fighter, 'source', source);

            fs.writeFile(dirFighters, JSON.stringify(data), (err) => {
                if (err) return rejective(err);
                resolve();
            });
        })
    })
}