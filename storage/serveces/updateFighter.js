const updateFighterDetails = require('../repositories/updateFighterDetails');
const updateFighter = require('../repositories/updateFighter');

module.exports = async (fighter, id) => {

    await updateFighter(id, fighter.name, fighter.source);
    return await updateFighterDetails(id, fighter.name, fighter.source, fighter.attack, fighter.defense, fighter.health);

}