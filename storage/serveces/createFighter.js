const uniqid = require('uniqid');
const createFighter = require('../repositories/createFighter');
const writeToFighters = require('../repositories/writeToFighters');
const validateFighter = require('../helpers/validateFighter');



module.exports = async (fighter) => {
    if (!validateFighter(fighter))
        throw new Error('Not valid data');
    const ID = uniqid.time();
    fighter = {
        _id: ID,
        name: fighter.name,
        source: fighter.source,
        attack: fighter.attack,
        defense: fighter.defense,
        health: fighter.health
    }
    await writeToFighters(fighter);
    return await createFighter(fighter);
};