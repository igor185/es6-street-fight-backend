const getFighter = require('../repositories/getFighter');
module.exports = async (id) => {
    return (await getFighter(id));
}