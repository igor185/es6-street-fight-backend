const getFighters = require('../repositories/getFighters');
module.exports = async () => {
    return (await getFighters());
}