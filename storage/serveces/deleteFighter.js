const deleteFighterDetails = require('../repositories/deleteFighterDetails');
const deleteFighterFromAll = require('../repositories/deleteFighterFromAll');


module.exports = async (id) => {
    await deleteFighterFromAll(id);
    return await deleteFighterDetails(id);
}