module.exports = {
    getFighters: require('./serveces/getFighters'),
    getFighter: require('./serveces/getFighter'),
    createFighter: require('./serveces/createFighter'),
    deleteFighter: require('./serveces/deleteFighter'),
    updateFighter: require('./serveces/updateFighter')
}