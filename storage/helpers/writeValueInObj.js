module.exports = (obj, key, param) => {
    if (!param) return;
    obj[key] = param;
}