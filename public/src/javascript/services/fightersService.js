import {
  callApi,
} from '../helpers/apiHelper';


class FightersService {
  async getFighters() {
    try {
      const endpoint = '/fighter'
      const apiResult = await callApi(endpoint, 'GET');
      return apiResult.content;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id) {
    try {
      const endpoint = `fighter/${_id}`
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult.content;
    } catch (error) {
      throw error;
    }
  }
}

export const fightersService = new FightersService();